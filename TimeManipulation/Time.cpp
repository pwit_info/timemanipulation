﻿#include "stdafx.h"
#include "Time.h"


int Time::toSeconds() const
{
	return 60 * 60 * hour + 60 * minute + sec;
}

bool Time::isValid24HourTime(unsigned char hour, unsigned char minute, unsigned char sec)
{
	return ((hour < 24) && (minute < 60) && (sec < 60));
}

Time::Time(unsigned char hour, unsigned char minute, unsigned char sec): hour(hour),
                                                                         minute(minute),
                                                                         sec(sec)
{
	if (!isValid24HourTime(hour, minute, sec))
		throw InvalidTimeException();
}



Time operator+(const Time& t1, const Time& t2)
{
	return t1.addSeconds(t2.toSeconds());
}


Time::Time()
{

}

Time::~Time()
{
}

Time Time::addHours(int hours) const
{
	auto newHours = (this->hour + hours);
	return Time( newHours % 24, this->minute, this->sec);
}

Time Time::addMinutes(int minutes) const
{
	auto newMinutes = (this->minute + minutes);
	auto newHours = this->hour + newMinutes / 60;
	return Time(newHours % 24, newMinutes % 60, this->sec);
}

Time Time::addSeconds(int i) const
{
	auto newSeconds = this->sec + i;
	auto newMinutes = this->minute + newSeconds / 60;
	auto newHours = this->hour + newMinutes / 60;
	return Time(newHours % 24, newMinutes % 60, newSeconds % 60);
}

bool operator<(const Time& lhs, const Time& rhs)
{
	if (lhs.hour < rhs.hour)
		return true;
	if (rhs.hour < lhs.hour)
		return false;
	if (lhs.minute < rhs.minute)
		return true;
	if (rhs.minute < lhs.minute)
		return false;
	return lhs.sec < rhs.sec;
}

bool operator<=(const Time& lhs, const Time& rhs)
{
	return !(rhs < lhs);
}

bool operator>(const Time& lhs, const Time& rhs)
{
	return rhs < lhs;
}

bool operator>=(const Time& lhs, const Time& rhs)
{
	return !(lhs < rhs);
}

bool operator==(const Time& lhs, const Time& rhs)
{
	return lhs.hour == rhs.hour
		&& lhs.minute == rhs.minute
		&& lhs.sec == rhs.sec;
}

bool operator!=(const Time& lhs, const Time& rhs)
{
	return !(lhs == rhs);
}
