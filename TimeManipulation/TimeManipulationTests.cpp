
#include "stdafx.h"
#include "gtest/gtest.h"
#include "Time.h"

TEST(TimeClassTest, Creation)
{
	Time *t = new Time();

	delete t;
}

TEST(TimeClassTest, ExplicitCreation)
{
	Time *t = new Time(13, 25, 30);

	delete t;
}

TEST(TimeClassTest, ExplicitCreationFails)
{
	ASSERT_ANY_THROW(Time t(25, 0, 0));  
}

TEST(TimeClassTest, TimesEqual)
{
	Time t1(13, 25, 30);
	Time t1p(13, 25, 30);

	EXPECT_EQ(t1, t1p);
}

TEST(TimeClassTest, TimeComparison)
{
	Time t1(0, 0, 0), t2(0, 0, 1);
	
	EXPECT_TRUE(t1 < t2);
}


TEST(TimeClassTest, AddHours)
{
	Time t1(23, 0, 0);

    EXPECT_EQ(Time(0, 0, 0), t1.addHours(1));
	EXPECT_EQ(t1, t1.addHours(24));

}

TEST(TimeClassTest, AddMinutes)
{
	Time t1(0, 59, 0);

	EXPECT_EQ(Time(1, 0, 0), t1.addMinutes(1));
	EXPECT_EQ(Time(1, 0, 0), t1.addMinutes(60*24 + 1));
}

TEST(TimeClassTest, AddSeconds)
{
	Time t1(0, 59, 59);

	EXPECT_EQ(Time(1, 0, 0), t1.addSeconds(1));
	EXPECT_EQ(Time(1, 0, 0), t1.addSeconds(60 * 60 * 24 + 1));
}

TEST(TimeClassTest, AddTimes)
{
	Time t1(0, 0, 1), t2(0,0,1);

	EXPECT_EQ(Time(0, 0, 2), t1 + t2);

}