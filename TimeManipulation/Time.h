﻿#pragma once

#include<exception>

class InvalidTimeException: public std::exception
{
public:
	virtual char const* what() const override
	{
		return "Hour::minute::second do not form proper 24h time spec.";
	}
};

class Time
{
	friend bool operator<(const Time& lhs, const Time& rhs);

	friend bool operator<=(const Time& lhs, const Time& rhs);

	friend bool operator>(const Time& lhs, const Time& rhs);

	friend bool operator>=(const Time& lhs, const Time& rhs);

	friend bool operator==(const Time& lhs, const Time& rhs);

	friend bool operator!=(const Time& lhs, const Time& rhs);

	int toSeconds() const;
	friend Time operator+(const Time& t1, const Time& t2);

	unsigned char hour, minute, sec;


public:
	bool isValid24HourTime(unsigned char hour, unsigned char minute, unsigned char sec);
	Time(unsigned char hour, unsigned char minute, unsigned char sec);

	Time();

	~Time();
	Time addHours(int hour) const;
	Time addMinutes(int i) const;
	Time addSeconds(int i) const;
};
